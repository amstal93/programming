# Local IspellDict: en
#+STARTUP: showeverything

#+SPDX-FileCopyrightText: 2018-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: git, decentralization, version, version control system, source code management, fork, clone, merge, commit, branch, git workflow, lightweight markup language

# This file uses a macro gitexercise, which needs to defined
# elsewhere.  In the simplest case, this could just be the text
# “Exercise”.  Better yet, it points to real instructions based on
# texts/Git-Workflow-Instructions.org, see there:
# https://gitlab.com/oer/oer-courses/cacs/blob/master/Git-Introduction.org

* Introduction
** Learning Objectives
   :PROPERTIES:
   :CUSTOM_ID: learning-objectives-git
   :reveal_extra_attr: data-audio-src="https://oer.gitlab.io/audio/learning-objectives.ogg"
   :END:
   - Discuss benefits and challenges of [[#vcs][version control systems]]
     (e.g., in the context of university study) and
     [[#vcs-review][contrast]]
     [[https://en.wikipedia.org/wiki/Decentralization][decentralized]] ones
     with centralized ones
   - Explain [[#states-review][states of files under Git]] and apply commands to manage them
   - Explain [[#feature-branch-workflow][Feature Branch Workflow]] and apply it
     in sample scenarios
   - Edit simple [[#markdown][Markdown]] documents
#+INCLUDE: "~/.emacs.d/oer-reveal-org/learning-objectives-notes.org"

** Core Questions
   #+ATTR_REVEAL: :frag (appear)
   - How to *collaborate* on shared documents as distributed team?
     {{{reveallicense("./figures/screenshots/magit.meta","55rh")}}}
     - Consider *multiple* people working on *multiple* files
       - Potentially in *parallel* on the *same* file
       - Think of group exercise sheet, project documentation, source
         code
   - How to keep track of who changed what why?
   - How to support unified/integrated end result?

** Your Experiences?
   - Briefly write down your own experiences.
     #+ATTR_REVEAL: :frag (appear)
     - Did you collaborate on documents
       - by sending them via e-mail,
       - by using shared (cloud) storage (e.g.,
         [[https://hochschulcloud.nrw/en/hilfe/webinterface/simultan.html][Sciebo with OnlyOffice]], Google),
       - by using collaborative editors (e.g.,
         [[https://hochschulcloud.nrw/en/hilfe/webinterface/simultan.html][Sciebo with OnlyOffice]],
         [[https://pad.riseup.net/][Etherpad]],
         [[https://pad.uni-muenster.de/][HedgeDoc]], Overleaf)
       - by using version control systems (e.g., Git, SVN),
       - by using other means?
     - Why did you choose what alternative?  What challenges arose?  Do
       you bother to read Terms of Service when you entrust “your”
       documents and thoughts (each individual keystroke, including “deleted”
       parts) to third parties (e.g., in the cloud)?

** Version Control Systems (VCSs)
   :PROPERTIES:
   :CUSTOM_ID: vcs
   :END:
   - Synonyms: Version/source code/revision control system, source
     code management (VCS, SCM)
   - Collaboration on *repository* of documents
     - Each document going through various versions/revisions
       - Each document improved by various authors
         - April 2012, Linux kernel 3.2: [[https://www.linux.com/training-tutorials/counting-contributions-who-wrote-linux-32/][1,316 developers from 226 companies]]

*** Major VCS features
    #+ATTR_REVEAL: :frag (appear)
    - VCS keeps track of *history*
      - Who changed what why when?
        {{{reveallicense("./figures/abstract/arrows-2033963_1920.meta","35rh")}}}
      - Restore/inspect old versions if necessary
    - VCS supports *merging* of versions into *unified/integrated*
      version
      - Integrate intermediate versions of single file with changes by
  	multiple authors
    - Copying of files is *obsolete* with VCSs
      - Do *not* create copies of files with names such as
	~Git-Intro-Final-1.1.txt~ or
	~Git-Intro-Final-reviewed-Alice.txt~
	- Instead, use VCS mechanism, e.g., use
          [[https://git-scm.com/book/en/v2/Git-Basics-Tagging][tags]] with Git

* Git Concepts

** Git: A Decentralized VCS
   - Various VCSs exist
     - E.g.: [[color:darkgreen][Git]], [[color:darkgreen][BitKeeper]],
       [[color:darkred][SVN]], [[color:darkred][CVS]]
       - (Color code: [[color:darkgreen][decentralized]],
         [[color:darkred][centralized]])
   - Git created by Linus Torvalds for the development of the [[https://www.kernel.org/][kernel Linux]]
     - Reference: [[https://git-scm.com/book/en/v2][Pro Git book]]
       {{{reveallicense("./figures/logos/Git-Logo-2Color.meta","15rh")}}}
     - Git as example of *[[https://en.wikipedia.org/wiki/Decentralization][decentralized]]* VCS
       - Every author has *own copy* of all documents and their history
       - Supports *offline* work without server connectivity
         - Of course, collaboration requires network connectivity
       - Distributed trust/control/visibility/surveillance

** Key Terms: Fork, Commit, Push, Pull
   :PROPERTIES:
   :CUSTOM_ID: git-key-terms
   :reveal_extra_attr: data-audio-src="./audio/git-key-terms-1.ogg"
   :END:
   - *Fork/clone* repository: Create copy of repository
     {{{reveallicense("./figures/icons/cloned-folder.meta","10rh")}}}
     - Clone: Create copy of remote repository on your machine
     - Fork: Create copy within online Git platform; then clone that
   #+ATTR_REVEAL: :frag appear :audio ./audio/git-key-terms-2.ogg
   - *Commit* (aka check-in)
     {{{reveallicense("./figures/icons/changed-folder.meta","20rh")}}}
     - Make (some or all) changes permanent; announce them to version
       control system
     - *Push*: Publish (some or all) commits to remote repository
       - Requires authorization
     - *Fetch* (*pull*): Retrieve commits from remote repository (also
       merge them)
   #+begin_notes
Some Git repositories are /public/, which means that everyone is
allowed to read and copy their contents.  For example, this is the
case for free software projects and open educational resources.
Obviously, owners of projects want to keep control over their
repositories, so they do /not/ allow random people to /change/
files.

The Git command line operation to copy a repository to your local
machine is called ~git clone~.  This operation downloads all
files and the history of some project, e.g.:
~git clone https://gitlab.com/oer/cs/programming.git~

As you own the cloned repository on your local machine, you can change
everything to your liking.  With ~git commit~, you record changes as
permanent, to be remembered by the version control system.  E.g.,
[[https://gitlab.com/oer/cs/programming/-/commit/791ef5c4e32ba1283d5c4739bf20c2d94a3e1110][this commit]]
shows a simple improvement of wording while
[[https://gitlab.com/oer/cs/programming/-/commit/6df5fc807b0f9bacb0f9aa44674f65556c3e7fbb][that one]]
introduces larger changes of slides.

To publish local commits to a repository, use ~git push~.  However, if
you just cloned someone else’s source project, you will /not/ be
allowed to integrate your commits into the source project.  Thus, ~git
push~ would fail.  In contrast, you can push to your own repositories
(and those where you were granted appropriate permissions).

On Web based Git platforms such as GitLab, you can create your own
fresh projects, to which you are allowed to push commits.

To contribute to someone else’s project, such platforms provide a
so-called /fork/ operation, creating a full copy of the source, which
is also called /upstream/.  You can clone your fork to your own
machine and apply commits.  This time, however, you own the repository
from which you cloned and you are allowed to push commits to that
repository.

Also, as we will see later on, such platforms provide so-called merge
requests (or pull requests), with which you can propose commits
on your forked project to be integrated into the upstream project.

Also, if you clone some repository you receive all commits up to that
point in time.  Over time, more commits may be applied to the
repository, and Git offers fetch and pull operations to retrieve those.
   #+end_notes

** Key Terms: Branch, Merge
   :PROPERTIES:
   :CUSTOM_ID: git-branches
   :reveal_extra_attr: data-audio-src="./audio/git-branches.ogg"
   :END:
   - *Branches*
     {{{reveallicense("./figures/git/git-branches.meta","40rh")}}}
     - Alternative versions of documents, on which to commit
       - Without being disturbed by changes of others
       - Without disturbing others
         - You can share your branches if you like, though
   - *Merge*
     - Combine changes of one branch into another branch
       - May or may not need to [[https://opensource.com/article/20/4/git-merge-conflict][resolve conflicts]]
   - (Don’t worry if this seems abstract, we’ll try this out.)
   #+begin_notes
With VCSs, different independent developments can happen at the same
time in a single project.  For example, while one team member adds
some feature to the UI, a second one might fix a bug, and a third one
might improve the backend.  To isolate those changes from each other,
they can take place in so-called /branches/, where the default branch is
called ~master~ (or ~main~) and represents a stable version.  Later on, the
commits of other, “finished” branches can be merged into the ~master~
branch.

Note that here we see four branches, the ~master~ branch in blue, one
for ongoing development in purple and two feature branches in green.
The feature branches were created from different states of the
development branch: the first one, let’s call it ~f1~, contains the
changes of one purple commit; the second one, say ~f2~, contains two
purple commits.  Note that ~f2~ introduces two commits while the
purple branch independently advances with another commit, before ~f2~
is /merged/ into the purple branch with a so-called /merge commit/.
At this point in time, the changes of all commits of ~f2~ are
integrated into the purple branch, and ~f2~ itself is no longer
particularly interesting.

For some point in the future, we should expect ~f1~ to be merged into
the purple branch and the purple branch to be merged into ~master~.
   #+end_notes

** Git explained by Linus Torvalds
   @@html:<video controls width="400" height="300" data-src="https://archive.org/download/LinusTorvaldsOnGittechTalk/LinusTorvaldsOnGittechTalk.ogv#t=460"></video>@@

   - [[https://archive.org/details/LinusTorvaldsOnGittechTalk][Video at archive.org]]
     (Tech Talk, 2007, by Google Talks under
     [[https://creativecommons.org/licenses/by-nc-sa/3.0/][CC BY-NC-SA 3.0]])
     - Total length of 84 minutes, suggested viewing: 7:40 to 29:00

*** Review Questions
   :PROPERTIES:
   :CUSTOM_ID: vcs-review
   :END:
    Prepare answers to the following questions
    - What is the role of a VCS (or SCM, in Torvalds’ terminology)?
    - What differences exist between decentralized and centralized VCSs?
      - By the way, Torvalds distinguishes centralized from distributed
        SCMs.  I prefer “decentralized” over “distributed”.  You?


* Git Basics
** In-Browser Tutorial
   :PROPERTIES:
   :CUSTOM_ID: git-tutorial
   :END:
   - Some students recommended this tutorial to try out Git commands in browser:
     https://learngitbranching.js.org/
     - Several levels of the tutorial cover Git commands that appear
       on [[#git-demo][later slides]]
       - Tab “Main”, Level “1: Introduction to Git Commits” introduces
         commit, branch, merge, rebase
       - Tab “Remote”, Level “1: Clone Intro” introduces clone, fetch,
         pull, push

** Getting Started
   - [[local:./texts/GitLab-Quickstart.html][Quickstart for Git installation and setup for GitLab]]
     #+ATTR_REVEAL: :frag appear
     - (Actually, you can only follow sections 4 and 5 after Git
       accounts have been set up)
     - At least, [[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git][install Git]]
       and perform [[https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup][first-time setup]]
   #+ATTR_REVEAL: :frag appear
   - You may use Git without a server
     - Run ~git init~ in any directory
       - Keep track of your own files
     - By default, you work on a branch called ~main~ or ~master~
       - That branch is not more special than any other branch you may create
       - (The term “master” is
         [[https://sfconservancy.org/news/2020/jun/23/gitbranchname/][offensive]];
         migration to “main” is under way in lots of places)

** Accessing Remote Repositories
   :PROPERTIES:
   :CUSTOM_ID: remote-access
   :END:
   - *Download* files from public repository: ~clone~
     - ~git clone https://gitlab.com/oer/cs/programming.git~
       - Change into that directory: ~cd programming~
         - Try out Git commands (but not ~git push~, which you are not
           allowed here)
       - Later on, ~git pull~ merges changes to bring your copy up to date
   - *Contribute* to remote repository
     - Push commits as explained [[#git-key-terms][earlier]] and
       revisited [[#git-remote][later on]]

*** A quick check
#+REVEAL_HTML: <script data-quiz="quizGitInstalled" src="quizzes/git-installed.js"></script>

** First Steps with Git
   :PROPERTIES:
   :CUSTOM_ID: git-demo
   :END:
   - Prerequisites
     - You [[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git][installed Git]]
     - You performed the
       [[https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup][First-time Git setup]]
   #+ATTR_REVEAL: :frag appear
   - Part 0
     - Create repository or clone one
       - ~git clone https://gitlab.com/oer/cs/programming.git~
       - Creates directory ~programming~
         - Change into that directory
         - Note presence of “real” contents and of sub-directory ~.git~ (with Git meta-data)

*** Part 1: Inspecting Status
    :PROPERTIES:
    :CUSTOM_ID: git-status
    :END:
    - Execute ~git status~
      - Output includes current branch and potential changes
    - Open some file in text editor and improve it
      - E.g., add something to ~Git-Introduction.org~
    - Create a new file, say, ~test.txt~
    - Execute ~git status~ again
      - Output indicates
        - ~Git-Introduction.org~ as *not staged* and *modified*
        - ~test.txt~ as *untracked*
        - Also, follow-up commands are suggested
          - ~git add~ to stage for commit
          - ~git checkout~ to discard changes

*** Part 2: Staging Changes
    :PROPERTIES:
    :CUSTOM_ID: git-stage
    :END:
    - Changes need to be ~staged~ before commit
      - ~git add~ is used for that purpose
      - Execute ~git add Git-Introduction.org~
      - Execute ~git status~
        - Output indicates ~Git-Introduction.org~ as *to be committed*
          and *modified*
    - Modify ~Git-Introduction.org~ more
    - Execute ~git status~
      - Output indicates ~Git-Introduction.org~ as
        - *To be committed* and *modified*
          - Those are your changes added in Part 1
        - As well as *not staged* and *modified*
          - Those are your changes of Part 2

*** Part 3: Viewing Differences
    :PROPERTIES:
    :CUSTOM_ID: git-diff
    :END:
    - Execute ~git diff~
      - Output shows changes that are not yet staged
        - Your changes of Part 2
    - Execute ~git diff --cached~
      - Output shows difference between staged changes and last
        committed version
    - Execute ~git add Git-Introduction.org~
    - Execute both ~diff~ variants again
      - Lots of other variants exits
        - Execute ~git help diff~
        - Similarly, *help* for other ~git~ commands is available

*** Part 4: Committing Changes
    :PROPERTIES:
    :CUSTOM_ID: git-commit
    :END:
    - Commit (to be committed) changes
      - Execute ~git commit -m "<what was improved>"~
        - Where ~<what was improved>~ should be meaningful text
        - [[https://chris.beams.io/posts/git-commit/][50 characters or less]]
    - Execute ~git status~
      - Output no longer mentions ~Git-Introduction.org~
        - Up to date from Git’s perspective
      - Output indicates that your branch advanced; ~git push~
        suggested for follow-up
    - Execute ~git log~ (press ~h~ for help, ~q~ to quit)
      - Output indicates commit history
      - Note your commit at top

*** Part 5: Undoing Changes
    :PROPERTIES:
    :CUSTOM_ID: git-reset
    :END:
    - Undo premature commit that only exists *locally*
      - Execute ~git reset HEAD~~
        - (*Don’t* do this for commits that exist in remote places)
      - Execute ~git status~ and ~git log~
        - Note that state before commit is restored
        - May apply more changes, commit later
    - Undo ~git add~ with ~git reset~
      - Execute ~git add Git-Introduction.org~
      - Execute ~git reset Git-Introduction.org~
    - Restore committed version
      - Execute ~git checkout -- <file>~
      - *Warning*: Local changes are *lost*

*** Part 6: Stashing Changes
    :PROPERTIES:
    :CUSTOM_ID: git-stash
    :END:
    - Save intermediate changes without commit
      - Execute ~git stash~
        - If you performed ~git checkout ...~ on previous slide,
          change some file first
      - Execute ~git status~ and find yourself on previous commit
    - Apply saved changes
      - Possibly on different branch or after ~git pull~
      - Execute ~git stash apply~
        - May lead to conflicts, to be resolved manually

*** Part 7: Branching
    :PROPERTIES:
    :CUSTOM_ID: git-branching
    :END:
    - Work on different branch
      - E.g., introduce new feature, fix bug, solve task
      - Execute ~git checkout -b testbranch~
        - Option ~-b~: *Create* new branch and switch to it
          - (Leave out for switch to existing branch)
      - Execute ~git status~ and find yourself on new branch
        - With uncommitted modifications from ~main~ (or ~master~)
        - Change more, commit on branch
        - Later on, [[#merge-vs-rebase][merge or rebase]] with ~main~
      - Execute ~git checkout main~ and ~git checkout testbranch~ to
        switch branches
        - (Newer versions of ~git~ know
          [[https://git-scm.com/docs/git-switch][~git switch~]] for
          the same purpose)

*** Remotes (1)
    :PROPERTIES:
    :CUSTOM_ID: git-remote
    :END:
    - Show remote repositories, whose changes you track:
      - ~git remote -v~
        - By default, remote after ~git clone~ is called ~origin~
        - No remote exists after ~git init~
        - For a forked project, one usually
          [[https://www.atlassian.com/git/tutorials/git-forks-and-upstreams][adds an ~upstream~ remote]]
          (see next two slides)
    - Contribute to project, two variants
      1. Operation ~push~ (requires permission)
         - You can ~push~ to your own projects
         - E.g., push new branch to remote ~origin~:
           - ~git push -u origin testbranch~
      2. Use merge/pull requests for other projects (next slide)

*** Remotes (2)
    :PROPERTIES:
    :CUSTOM_ID: feature-branch-workflow
    :END:
    - Contribute to some project, the ~upstream~
      ([[https://git-scm.com/book/en/v2/GitHub-Contributing-to-a-Project][section in Pro Git]])
      - Projects follow different [[#git-workflow][workflows]]; read
        project’s contribution instructions first
      - E.g., *Feature Branch Workflow*
        - Fork upstream project (in GUI)
          - Which creates *your own* project with full permissions
        - Clone it
        - Create separate branch for *each* independent contribution
          - E.g., bug fix, new feature, improved documentation
          - Commit, push branch (to fork)
        - In GUI, open *merge request* (GitLab) or *pull request*
          (GitHub) for branch
          - If accepted, its changes are merged into upstream project

*** Remotes (3)
    :PROPERTIES:
    :CUSTOM_ID: git-remote-upstream
    :reveal_extra_attr: data-audio-src="./audio/git-remote-upstream.ogg"
    :END:
    - When merge request was accepted upstream, maybe update your
      fork to mirror upstream’s state
      - Goal: Update your ~master~ branch based on upstream’s ~master~ branch
      - Approach
        - Set up source project as remote ~upstream~:
          - ~git remote add upstream <HTTPS-URL of source project>~
        - Fetch ~upstream~: ~git fetch upstream~
        - Integrate ~upstream/master~ into your ~master~, maybe with
          [[#merge-vs-rebase][rebase]]:
          - ~git checkout master~
          - ~git rebase upstream/master~
        - Push updated master to your fork: ~git push~
    #+begin_notes
A point that might be confusing at first is the following one: We talk
about /different/ ~master~ (and other) branches.  When you fork a
project, the source project and your fork both contain a ~master~
branch, and while those branches contain the same commits initially,
they may /diverge/ over time: You may apply commits to your ~master~,
while the source project also evolves.  Thus, those two branches are
really different things.  As you see on the previous and this slide,
we can use names such as ~origin~ and ~upstream~ to name
different remote repositories, which allows us to distinguish
different versions of branches such as ~origin/master~ from
~upstream/master~.

In addition, when you clone a repository, you create a ~master~ branch
on your local machine, which again is different from the one in the
remote repository.  You may change that local ~master~ branch, while
others might change ~master~ in the remote repository.  Thus, those
two branches are different things, and we use ~push~ and ~fetch~ or
~pull~ to synchronize them.

It is important to note that commits on a single branch are ordered
/linearly/.  Thus, suppose that you clone a remote repository, where
the latest commit on ~master~ is C0.  You then add commit C1 to your
local ~master~ branch (after C0), and someone else concurrently adds
C2 to the remote ~master~ (after C0).  In this situation, attempts to
~push~ the local ~master~ branch would /fail/ (because the two commits
C1 and C2 both have the same parent commit C0 and are not ordered with
respect to each other).  In this situation, one needs to ~merge~ or to
~rebase~ to integrate both master branches (which you will explore in
the exercises).

Workflows based on branches (as mentioned on the
[[#feature-branch-workflow][previous slide]])
help to gain control over such situations.
    #+end_notes

*** Review Questions
   :PROPERTIES:
   :CUSTOM_ID: states-review
   :END:
    - As part of [[#git-demo][First Steps with Git]], ~git status~
      inspects repository, in particular file *states*
      - Recall that files may be ~untracked~, if they are located
        inside a Git repository but not managed by Git
      - Other files may be called ~tracked~
    - Prepare answers to the following questions
      - Among the ~tracked~ files, which states can you identify from
        the demo?  Which commands are presented to perform what state
        transitions?
      - Optional: Draw a diagram to visualize your findings

** Merge vs Rebase
   :PROPERTIES:
   :CUSTOM_ID: merge-vs-rebase
   :END:
   - Commands ~merge~ and ~rebase~ both unify two [[#git-branches][branches]]
   - Illustrated subsequently
     - Same unified file contents in the end, but different views of history

*** Merge vs Rebase (1)
    - Suppose you created branch for new ~feature~ and committed on that
      branch; in the meantime, somebody else committed to ~master~

    {{{reveallicense("./figures/git/forked-commit-history.meta","50rh")}}}

*** Merge vs Rebase (2)
    :PROPERTIES:
    :CUSTOM_ID: git-merge
    :END:
    - Merge creates *new* commit to combine both branches
      - Including all commits
      - Keeping parallel history

    {{{reveallicense("./figures/git/merged-feature.meta","40rh")}}}

*** Merge vs Rebase (3)
    :PROPERTIES:
    :CUSTOM_ID: git-rebase
    :END:
    - Rebase rewrites ~feature~ branch on ~master~
      - Applies *local* commits of ~feature~ on ~master~
      - Cleaner end result, but branch’s history lost/changed
        - Only do this for local commits (i.e., before you pushed ~feature~)
          - Rebase changes history, so use ~merge~ for remote branches

    {{{reveallicense("./figures/git/rebased-feature.meta","40rh")}}}

** Git Workflows
    :PROPERTIES:
    :CUSTOM_ID: git-workflow
    :END:
   - For collaboration, team needs to agree on *Git workflow*
     - [[beyond:https://www.atlassian.com/git/tutorials/comparing-workflows][Several alternatives]] exist
   - [[https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow][Feature Branch Workflow]]
     may be your starting point
     - [[#feature-branch-workflow][Sketched previously]]

*** Sample Commands
    :PROPERTIES:
    :CUSTOM_ID: git-commands
    :END:
#+BEGIN_SRC bash
git clone <project-URI>
# Then, later on retrieve latest changes:
git fetch origin
# See what to do, maybe pull when suggested in status output:
git status
git pull
# Create new branch for your work and switch to it:
git checkout -b nameForBranch
# Modify/add files, commit (potentially often):
git add newFile
git commit -m "Describe change"
# Push branch:
git push -u origin nameForBranch
# Ultimately, merge or rebase branch nameForBranch into branch master
git checkout master
git merge nameForBranch
# If conflict, resolve as instructed by git, commit.  Finally push:
git push
#+END_SRC

* GitLab

** GitLab Overview
   - Web platform for Git repositories
     - [[https://about.gitlab.com/]]
     - Free software, which you could run on your own server
   - Manage Git repositories
     - Web GUI for forks, commits, pull requests, issues, and much more
     - Notifications for lots of events
       - Not enabled by default
     - So-called Continuous Integration (CI) runners to be executed upon
       commit
       - Based on Docker images
       - Build and test your project (build executables, test them,
         deploy them, generate documentation, presentations, etc.)

** GitLab in Action
   - {{{gitexercise}}}

* Aside: Lightweight Markup Languages
** Lightweight Markup
   - Markup: “Tags” for annotation in text, e.g., indicate sections and
     headings, emphasis, quotations, …
   - [[basic:https://en.wikipedia.org/wiki/Lightweight_markup_language][Lightweight markup]]
     - ASCII-only punctuation marks for “tags”
     - Human readable, simple syntax, standard text editor sufficient
       to read/write
     - Tool support
       - Comparison and merge, e.g.,
         [[beyond:https://en.wikipedia.org/wiki/Merge_(version_control)#Three-way_merge][three-way merge]]
       - Conversion to target language (e.g. (X)HTML, PDF, EPUB, ODF)
         - Wikis, blogs
         - [[beyond:https://pandoc.org/][pandoc]] can convert between lots of languages

** Markdown
   :PROPERTIES:
   :CUSTOM_ID: markdown
   :END:
   - [[basic:https://en.wikipedia.org/wiki/Markdown][Markdown]]: A lightweight markup language
   - Every Git repository should include a README file
     - What is the project about?
     - Typically, ~README.md~ in Markdown syntax
   - Learning Markdown
     - [[beyond:https://www.markdowntutorial.com][In-browser tutorial]]
       (source code under [[https://github.com/gjtorikian/markdowntutorial.com/blob/master/LICENSE.txt][MIT License]])
     - [[basic:https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet][Cheatsheet]]
       (under CC BY 3.0)

** Org Mode
   - [[beyond:https://orgmode.org/][Org mode]]: Another lightweight markup language
     - My favorite one
   - For details see [[beyond:https://gitlab.com/oer/cs/programming/blob/master/Git-Introduction.org][source file for this presentation as example]]

* Conclusions
** Summary
   - VCSs enable collaboration on files
     - Source code, documentation, theses, presentations
   - Decentralized VCSs such as Git enable distributed, in particular
     offline, work
     - Keeping track of files’ states
       - With support for subsequent merge of divergent versions
     - Workflows may prescribe use of branches for pull requests
   - Documents with lightweight markup are particularly well-suited
     for Git management

** Where to go from here?
   - Version control is essential for DevOps
     - Combination of Development and Operations, see cite:JbA+16,WFW+19
     - Aiming for rapid software release cycles with high degree of
       automation and stability
   - Variant based on Git is called GitOps, see cite:Lim18
     - Self-service IT with proposals in pull requests (PRs)
     - Infrastructure as Code (IaC)

# Local Variables:
# indent-tabs-mode: nil
# oer-reveal-master: nil
# End:
