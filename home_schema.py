#!/usr/bin/python
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020-2021 Jens Lechtenbörger

u"""Sample use of FlexMatcher, https://github.com/biggorilla-gh/flexmatcher.

Clone or download FlexMatcher, place this file into subdirectory "examples",
run "python home_schema.py".

Actually, you may see errors concerning charDistClassifier.py:
- 'DataFrame' object has no attribute 'ix'
- 'DataFrame' object has no attribute 'as_matrix'
In that file, replace 'ix' with 'iloc' and 'as_matrix' with 'to_numpy'.
(Note that this file appends '..' to the path to use the source files of
FlexMatcher.  If you install flexmatcher as package, you need to change
a file in that package.)

The structure and some comments of this file are taken over from
"examples/movie_schemas.py" of FlexMatcher.

Sample data is inspired by an example in the slides for this textbook:
A. Doan, A. Halevy, Z. Ives. Principles of Data Integration,
Morgan Kaufmann, 2012.
https://research.cs.wisc.edu/dibook/

By default, FlexMatcher applies data and column name matching, which
leads to the expected result here.
To deactivate column name matching (and see its impact), make sure
that the if statement in line 85 of flexmatcher.py returns false
(e.g., replace 5 with 15):
https://github.com/biggorilla-gh/flexmatcher/tree/master/flexmatcher/flexmatcher.py#L85
"""

import sys
import pandas as pd
sys.path.append("..")

import flexmatcher

# Let's assume that the mediated schema has five attributes:
# price, contact_name, contact_phone, office, description

# Variables starting with "VALS" below are supposed to represent relations,
# with attributes specified in the first sublist and rows afterwards.

# Variables "DATA<X>_MAPPING" specify known mappings between relations and
# mediated schema.

VALS1 = [['Text', 'Agent', 'Telephone', 'OfficeFax', 'Price'],
         ['Penthouse in quiet neighborhood', 'Alice Wonderland',
          '(251) 432 895', '(251) 432 896', '900000'],
         ['Villa in countryside', 'Bob Builder',
          '(123) 5432 345', '(123) 5432 346', '1500000'],
         ['Downtown flat', 'Mallory Miller',
          '(436) 743 1324', '(436) 743 1325', '220000']]
HEADER = VALS1.pop(0)
DATA1 = pd.DataFrame(VALS1, columns=HEADER)
DATA1_MAPPING = {'Price': 'price', 'Telephone': 'contact_phone',
                 'Text': 'description',
                 'Agent': 'contact_name', 'OfficeFax': 'office'}

VALS2 = [['sell_price', 'contact_agent', 'agent_name', 'secretary',
          'extra_information'],
         ['350000', '(206) 634 9435', 'Dave Dull', '(206) 634 9436',
          'Beautiful yard'],
         ['230000', '(617) 335 4243', 'Eve Evergreen', '(617) 335 4244',
          'Close to seattle'],
         ['400000', '(251) 123 5467', 'Frank Furter', '(251) 123 5468',
          'Overlooking the harbor']]
HEADER = VALS2.pop(0)
DATA2 = pd.DataFrame(VALS2, columns=HEADER)
DATA2_MAPPING = {'sell_price': 'price', 'contact_agent': 'contact_phone',
                 'extra_information': 'description',
                 'agent_name': 'contact_name', 'secretary': 'office'}

VALS1B = [['Beschreibung', 'Makler', 'Telefon', 'Fax', 'Preis'],
          ['Penthouse in ruhiger Nachbarschaft', 'Makler Meier',
           '(251) 432 897', '(251) 432 898', '900000'],
          ['Villa auf dem Land', 'Immo Landliebe',
           '(123) 5432 347', '(123) 5432 348', '1500000'],
          ['Zentrale Stadtwohnung', 'Schöner Wohnen',
           '(436) 743 1327', '(436) 743 1328', '220000']]
HEADER = VALS1B.pop(0)
DATA1B = pd.DataFrame(VALS1B, columns=HEADER)
DATA1B_MAPPING = {'Preis': 'price', 'Telefon': 'contact_phone',
                  'Beschreibung': 'description',
                  'Makler': 'contact_name', 'Fax': 'office'}

VALS2B = [['sell_price', 'contact_phone', 'fax', 'name', 'specifics'],
          ['540000', '(543) 1267 754', '(543) 1267 755',
           'Micky Mouse', 'Terrific garden'],
          ['300000', '(432) 8734 437', '(432) 8734 438',
           'Mini Mouse', 'Close to Münster'],
          ['480000', '(213) 9357 123', '(213) 9357 124',
           'Donald Duck', 'Unusually quiet']]
HEADER = VALS2B.pop(0)
DATA2B = pd.DataFrame(VALS2B, columns=HEADER)
DATA2B_MAPPING = {'sell_price': 'price', 'contact_phone': 'contact_phone',
                  'fax': 'office', 'name': 'contact_name',
                  'specifics': 'description'}

VALS1C = [['Information', 'Contact', 'Fax', 'Name', 'Suggested_Price'],
          ['20-room villa', '(252) 632 8958', '(252) 632 8959',
           'John Jonsen', '9000000'],
          ['3-room penthouse', '(124) 6432 3458', '(124) 6432 3459',
           'Jens Jensen', '1800000'],
          ['3-room flat', '(437) 643 13248', '(437) 643 13249',
           'Lars Larsen', '200000']]
HEADER = VALS1C.pop(0)
DATA1C = pd.DataFrame(VALS1C, columns=HEADER)
DATA1C_MAPPING = {'Suggested_Price': 'price', 'Contact': 'contact_phone',
                  'Information': 'description', 'Fax': 'office',
                  'Name': 'contact_name'}

VALS2C = [['price', 'contact', 'office', 'agent_name', 'description'],
          ['360000', '(207) 634 9435', '(207) 634 9436',
           'Jim Beam', 'Nothing special'],
          ['240000', '(618) 335 4243', '(618) 335 4244',
           'Johnny Walker', 'Close to New York'],
          ['410000', '(252) 123 5467', '(252) 123 5468',
           'Joe Sixpack', 'Walking distance to university']]
HEADER = VALS2C.pop(0)
DATA2C = pd.DataFrame(VALS2C, columns=HEADER)
DATA2C_MAPPING = {'price': 'price', 'contact': 'contact_phone',
                  'description': 'description',
                  'office': 'office', 'agent_name': 'contact_name'}

# creating a list of dataframes and their mappings
SCHEMA_LIST = [DATA1, DATA2, DATA1B, DATA2B, DATA1C, DATA2C]
MAPPING_LIST = [DATA1_MAPPING, DATA2_MAPPING,
                DATA1B_MAPPING, DATA2B_MAPPING,
                DATA1C_MAPPING, DATA2C_MAPPING]

# creating the test dataset
# we assume that we don't know the mapping and we want FlexMatcher to find it.
VALS3 = [['sold_at', 'contact_agent', 'extra_info'],
         ['250000', '(305) 729 0831', 'Fantastic house'],
         ['320000', '(617) 253 1429', 'Great location'],
         ['100000', '(251) 83 38159', 'Former barrack']]
HEADER = VALS3.pop(0)
DATA3 = pd.DataFrame(VALS3, columns=HEADER)


# Using Flexmatcher
print('Creating flexmatcher.')
FM = flexmatcher.FlexMatcher(SCHEMA_LIST, MAPPING_LIST, sample_size=100)
print('Training.')
FM.train()
print('Predicting.')
PREDICTED_MAPPING = FM.make_prediction(DATA3)

# printing the predictions
print('FlexMatcher predicted that "sold_at" should be mapped to ' +
      PREDICTED_MAPPING['sold_at'])
print('FlexMatcher predicted that "contact_agent" should be mapped to ' +
      PREDICTED_MAPPING['contact_agent'])
print('FlexMatcher predicted that "extra_info" should be mapped to ' +
      PREDICTED_MAPPING['extra_info'])
